# README #

### ZUWEISEUNG THEMENBEREICHE ###

selection: Tim, Sebastian
crossover: Daniel, Marius
mutation: Christoph, Niklas
bruteforce: Kevin
statistics / test: Moritz


## BUGLIST ##
### Offen ###

### TODO Liste ###


### Informationen ###

Herr Müller wird übers Wochenende eine XML Parser erstellen, damit man die "genetic_algorithm_knapsack.xml" lesen kann, wo die Szenarien definiert sind.
In der Configuration Datei steht dann unter der Variable "scenario", welches Szenario ausgeführt werden soll!
Somit ist folgender Ablauf für die Application.execute() Methode definiert:

- Szenario laden (Selectormethode, Crossingmethode, Mutationmethode)
- Genetischen Algorithmus ausführen
- Je Nach Szenario Bruteforce ausführen und vergleichen

@BodoKlappstuhl alias Moritz:
Bitte das eintragen des szenario und die fitnessValues erfolgreich in die Datenbank eintragen, damit man die genetischen Algorithmen testen kann