package selection;

import base.Knapsack;

/**
 * Created by Sebastian on 17.02.2017.
 */
public class RouletteWheelItem {
    Knapsack knapsack;
    float lowerLimit, upperLimit;

    public RouletteWheelItem(Knapsack knapsack, float lowerLimit, float upperLimit){
        this.knapsack   = knapsack;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;

        if(upperLimit < lowerLimit){
            throw new IllegalArgumentException("It is not possible to create a RouletteWheelItem with an upper limit lower than its lower limit");
        }
    }
}
