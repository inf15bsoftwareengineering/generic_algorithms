package selection;

import java.util.ArrayList;
import java.util.List;

import base.Knapsack;

public interface ISelection {
    List<Knapsack> doSelection(List<Knapsack> knapsacks);
}