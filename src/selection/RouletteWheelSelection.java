package selection;

import java.util.ArrayList;
import java.util.List;

import base.Knapsack;
import main.Configuration;

public class RouletteWheelSelection implements ISelection {

    public List<Knapsack> doSelection(List<Knapsack> knapsacks){
        ArrayList<RouletteWheelItem> rouletteList = new ArrayList<>(); //Rouletterad
        ArrayList<Knapsack> knapsacksNew = new ArrayList<>(); //ausgewaehlte Knappsacks

        /*bestimmen der RouletteWheelItems mit
         *  - dem jeweiligen Knappsack
         *  - der Untergrenze
         *  - der Obergrenze
         */
        if(knapsacks.size() >= 50) {

            float lastUpperBorder = 0.0f;
            for (int i = 0; i < (knapsacks.size()); i++) {
                float probability = knapsacks.get(i).getSelectionProbability();
                float upperLimit = lastUpperBorder + probability;

                rouletteList.add(new RouletteWheelItem(knapsacks.get(i), lastUpperBorder, upperLimit));
                lastUpperBorder += probability;
            }

            /*Auswahl von 50 Knappsacks
             *  - diese werden in die knappsacksNew Liste geschreiben
             *  - Liste wird returned
             */
            while (knapsacksNew.size() < 50) {
                double pointer = Configuration.instance.randomNumberGenerator.nextDouble();
                for (int a = 0; a < rouletteList.size(); a++) {
                    if (rouletteList.get(a).upperLimit > pointer && rouletteList.get(a).lowerLimit <= pointer) {
                        if(knapsacksNew.contains(rouletteList.get(a).knapsack)){
                            continue;
                        }

                        knapsacksNew.add(rouletteList.get(a).knapsack);
                        break;
                    }
                }
            }
        }
        return knapsacksNew;
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}