package selection;

import base.Knapsack;

public class KnapsackMatch {
    private Knapsack k1;
    private Knapsack k2;
    private Knapsack k3;

    public KnapsackMatch(Knapsack k1, Knapsack k2, Knapsack k3) {
        this.k1 = k1;
        this.k2 = k2;
        this.k3 = k3;
    }

    public Knapsack doMatch() {
        return this.max(this.k1, this.k2, this.k3);
    }

    private static Knapsack max(Knapsack... n) {
        int i = 0;
        int max = n[i].getTotalValue();
        Knapsack maxKnapsack = null;

        while (++i < n.length) {
            if (n[i].getTotalValue() > max) {
                max = n[i].getTotalValue();
                maxKnapsack = n[i];
            }
        }

        return maxKnapsack;
    }
}
