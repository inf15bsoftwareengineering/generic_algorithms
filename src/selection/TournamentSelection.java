package selection;

import java.util.*;

import base.Knapsack;
import main.Configuration;


public class TournamentSelection implements ISelection {
    public List<Knapsack> doSelection(List<Knapsack> knapsacks) {
        List<Knapsack> deepCopy = new ArrayList<>(knapsacks.size());
        for(Knapsack sack :knapsacks){
            deepCopy.add(sack.clone());
        }

        ArrayList<KnapsackMatch> knapsackMatching = new ArrayList<>();
        ArrayList<Knapsack> selectionResults = new ArrayList<>();

        //Generate random knapsack pairs
        while(deepCopy.size() > 0) {
            Knapsack[] knapsackPair = new Knapsack[3];
            for (int i = 0; i < 3; i++) {
                int maxValue = deepCopy.size(), selector;
                if(maxValue == 0) {
                    selector = 0;
                } else {
                    selector = this.generateRandomKnapsackSelector(maxValue);
                }
                knapsackPair[i] = deepCopy.get(selector);
                deepCopy.remove(knapsackPair[i]);
            }

            knapsackMatching.add(new KnapsackMatch(knapsackPair[0], knapsackPair[1], knapsackPair[2]));
        }

        //Do tournament selection
        for(KnapsackMatch match : knapsackMatching) {
            selectionResults.add(match.doMatch());
        }

        return selectionResults;
    }

    private int generateRandomKnapsackSelector(int maxValue) {
        return Configuration.instance.randomNumberGenerator.nextInt(maxValue);
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}

