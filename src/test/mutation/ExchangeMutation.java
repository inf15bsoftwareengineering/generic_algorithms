package test.mutation;

import base.Item;
import base.Knapsack;
import org.junit.Assert;
import org.junit.Test;


public class ExchangeMutation {
    @Test
    public void numberOfChanges() {
        Knapsack knapsack = new Knapsack();
        for(int i = 0; i < 20; i ++){
            knapsack.addItem(new Item(String.valueOf(i), 0 , 0));
        }
        Knapsack mutatedKnapsack = new mutation.ExchangeMutation().doMutation(knapsack);

        Assert.assertEquals(knapsack.getItems().size(), mutatedKnapsack.getItems().size());
        int changes = 0;
        for(int i = 0; i < knapsack.getItems().size(); i++){
            if(!knapsack.getItems().get(i).equals(mutatedKnapsack.getItems().get(i))){
                changes++;
            }
        }

        Assert.assertEquals(2, changes);
    }

    @Test
    public void noShrinking(){
        Knapsack knapsack = new Knapsack();
        for(int i = 0; i < 20; i ++){
            knapsack.addItem(new Item(String.valueOf(i), 0 , 0));
        }
        Knapsack mutatedKnapsack = new mutation.ExchangeMutation().doMutation(knapsack);
        Assert.assertEquals(knapsack.getItems().size(), mutatedKnapsack.getItems().size());
    }
}