package test.mutation;

import base.Item;
import base.Knapsack;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScrambleMutation {
    @Test
    public void test() {
        Knapsack knapsack = new Knapsack();
        for(int i = 0; i < 20; i ++){
            knapsack.addItem(new Item(String.valueOf(i), 0 , 0));
        }
        Knapsack mutatedKnapsack = new mutation.ScrambleMutation().doMutation(knapsack);

        Assert.assertEquals(knapsack.getItems().size(), mutatedKnapsack.getItems().size());
    }
}