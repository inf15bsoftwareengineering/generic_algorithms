package test.selection;

import base.Item;
import base.Knapsack;
import base.Population;
import main.Application;
import main.Configuration;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class TournamentSelection {
    List<Item> allItems;
    @BeforeClass
    public static void initialize() {
        new Application().loadData();
        Configuration.instance.selection = new selection.TournamentSelection();
    }

    private Population generateTestPopulation() {
        //Generate new random population
        Population pop = new Population();
        for (int i = 0; i < Configuration.instance.populationSize; i++) {
            pop.getSelection().add(Knapsack.generateRandom());
        }
        pop.recalculate();
        return pop;
    }

    @Test
    public void selectionKnapsackSize() {
        Population pop = this.generateTestPopulation();
        List<Knapsack> knapsackResults = Configuration.instance.selection.doSelection(pop.getSelection());
        Assert.assertEquals(50, knapsackResults.size());
    }

    @Test
    public void selectionPopulationUnchanged() {
        Population pop = this.generateTestPopulation();
        List<Knapsack> knapsackResults = Configuration.instance.selection.doSelection(pop.getSelection());
        Assert.assertEquals(150, pop.getSelection().size());
    }
}