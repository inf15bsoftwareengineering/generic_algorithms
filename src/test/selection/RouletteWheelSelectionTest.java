package test.selection;

import base.Knapsack;
import base.Population;
import main.Application;
import main.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import selection.RouletteWheelSelection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebastian on 21.02.2017.
 */
public class RouletteWheelSelectionTest {

    @Test
    public void testRouletteWheelSelection(){
        Population pop = this.createInitialPopulation();
        List<Knapsack> results = Configuration.instance.selection.doSelection(pop.getSelection());
        Assert.assertEquals(50, results.size());
    }

    @Test
    public void testSizeOfInitialList(){
        Population pop = this.createInitialPopulation();
        List<Knapsack> results = Configuration.instance.selection.doSelection(pop.getSelection());
        Assert.assertEquals(150, pop.getSelection().size());
    }

    @BeforeClass
    public static void init(){
        new Application().loadData();
        Configuration.instance.selection = new RouletteWheelSelection();
    }

    private Population createInitialPopulation() {
        Population pop = new Population();
        for (int i = 0; i < Configuration.instance.populationSize; i++) {
            pop.getSelection().add(Knapsack.generateRandom());
        }
        pop.recalculate();
        return pop;
    }
}
