package test.base;


import base.Knapsack;
import base.Population;
import main.Application;
import main.Configuration;
import org.junit.Assert;
import org.junit.Test;

public class PopulationTest {


    @Test
    public void selectionPropability(){
        new Application().loadData();
        Population population = new Population();
        for (int i = 0; i < Configuration.instance.populationSize; i++) {
            population.getSelection().add(Knapsack.generateRandom());
        }

        double probability = 0.0;
        for(Knapsack k : population.getSelection()){
            probability += k.getSelectionProbability();
        }

        Assert.assertEquals("", 1.0, probability, 0.000001);
    }

}
