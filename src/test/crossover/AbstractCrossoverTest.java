package test.crossover;


import base.Item;
import base.Knapsack;
import main.Application;
import main.Configuration;
import org.junit.Assert;

import java.util.*;

public abstract class AbstractCrossoverTest {


    public void crossOverRatio() {
        Configuration.instance.crossoverRatio = 0;

        Knapsack parent1 = Knapsack.generateRandom();
        Knapsack parent2 = Knapsack.generateRandom();

        List<Knapsack> children = Configuration.instance.crossover.doCrossover(parent1, parent2);

        Assert.assertTrue(children.isEmpty());
    }

    public void childrenSize() {
        Configuration.instance.crossoverRatio = 1;

        Knapsack parent1 = createKnapsackWithItems(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        Knapsack parent2 = createKnapsackWithItems(12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);

        List<Knapsack> children = Configuration.instance.crossover.doCrossover(parent1, parent2);

        Assert.assertFalse(children.isEmpty());

        for (Knapsack child : children) {
            Assert.assertTrue(Configuration.instance.maxVolumeKnapsack >= child.getTotalSize());

            Set<Item> items = new HashSet<>();
            items.addAll(child.getItems());

            Assert.assertEquals(items.size(), child.getItems().size());
        }

        parent1 = new Knapsack();
        parent2 = new Knapsack();

        parent1.getItems().addAll(Application.getAllItemsFromCSV());
        parent2.getItems().addAll(Application.getAllItemsFromCSV());

        children = Configuration.instance.crossover.doCrossover(parent1, parent2);

        Assert.assertTrue(children.isEmpty());
    }

    public void noShrinking() {
        Configuration.instance.crossoverRatio = 1;

        Knapsack parent1 = createKnapsackWithItems(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        Knapsack parent2 = createKnapsackWithItems(12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);

        List<Knapsack> children = Configuration.instance.crossover.doCrossover(parent1, parent2);
        Assert.assertEquals(2, children.size());
        checkAllItemsStillAvailable(parent1, parent2, children.get(0), children.get(1));
    }

    public void checkAllItemsStillAvailable(Knapsack parent1, Knapsack parent2, Knapsack child1, Knapsack child2) {
        List<Item> parentItems = new ArrayList<>();
        List<Item> childItems = new ArrayList<>();

        parentItems.addAll(parent1.getItems());
        parentItems.addAll(parent2.getItems());
        Collections.sort(parentItems);

        childItems.addAll(child1.getItems());
        childItems.addAll(child2.getItems());
        Collections.sort(childItems);

        Assert.assertEquals("There must be the same amount of items in both children and parents", parentItems.size(), childItems.size());
        for (int i = 0; i < parentItems.size(); i++) {
            Assert.assertEquals(parentItems.get(i), childItems.get(i));
        }
    }

    public Knapsack createKnapsackWithItems(int... itemNumbers) {
        Knapsack knapsack = new Knapsack();
        for (int i : itemNumbers) {
            knapsack.addItem(Application.getAllItemsFromCSV().get(i));
        }
        return knapsack;
    }
}
