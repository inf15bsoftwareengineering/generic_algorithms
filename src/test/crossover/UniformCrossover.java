package test.crossover;

import base.Item;
import base.Knapsack;
import main.Application;
import main.Configuration;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class UniformCrossover extends AbstractCrossoverTest {


    @BeforeClass
    public static void initialize() {
        new Application().loadData();
        Configuration.instance.crossover = new crossover.UniformCrossover();
    }

    @Test
    public void crossOverRatio() {
        super.crossOverRatio();
    }

    @Test
    public void childrenSize() {
        super.childrenSize();
    }

    @Test
    public void noShrinking() {
        super.noShrinking();
    }

    @Test
    public void sameAmountOfItemsFromParents() {
        Configuration.instance.crossoverRatio = 1;

        Knapsack parent1 = createKnapsackWithItems(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        Knapsack parent2 = createKnapsackWithItems(12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);

        List<Knapsack> children = Configuration.instance.crossover.doCrossover(parent1, parent2);

        Assert.assertEquals(2, children.size());
        for (Knapsack child : children) {
            int itemsFromParent1 = 0;
            int itemsFromParent2 = 0;
            for (Item item : child.getItems()) {
                if (parent1.getItems().contains(item)) {
                    itemsFromParent1++;
                }
                if (parent2.getItems().contains(item)) {
                    itemsFromParent2++;
                }
            }

            Assert.assertTrue(Math.abs(itemsFromParent1 - itemsFromParent2) <= 1);
        }

    }
}
