package test.crossover;

import main.Application;
import main.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;

public class KPointCrossover extends AbstractCrossoverTest{


    @BeforeClass
    public static void initialize() {
        new Application().loadData();
        Configuration.instance.crossover = new crossover.KPointCrossover();
    }

    @Test
    public void crossOverRatio() {
        super.crossOverRatio();
    }

    @Test
    public void childrenSize() {
        super.childrenSize();
    }

    @Test
    public void noShrinking() {
        super.noShrinking();
    }
}