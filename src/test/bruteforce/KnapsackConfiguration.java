package test.bruteforce;

import bruteforce.generators.IConfigurationGenerator;
import bruteforce.generators.RandomConfigurationGenerator;
import bruteforce.generators.SequentialConfigurationGenerator;
import org.junit.Test;

/**
 * Created by kevin on 17.02.2017.
 */
public class KnapsackConfiguration {
    @Test
    public void testGenerateRandomConfigurations() {
        IConfigurationGenerator generator = new RandomConfigurationGenerator();
    }

    @Test
    public void testGenerateSequentialConfigurations() {
        IConfigurationGenerator generator = new SequentialConfigurationGenerator();
    }
}
