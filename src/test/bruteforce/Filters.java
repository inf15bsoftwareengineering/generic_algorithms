package test.bruteforce;

import base.Knapsack;
import base.KnapsackConfiguration;
import bruteforce.filters.*;
import bruteforce.services.KnapsackService;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class Filters {
    @Test
    public void testAllFilter() {
        IKnapsacksFilter filter = new AllKnapsacksFilter();
        KnapsackService service = new KnapsackService();
        ArrayList<Knapsack> knapsacks = new ArrayList<>();

        // Create 8 Knapsacks for testing
        for(int i = 0; i < 8; i++) {
            knapsacks.add(service.create(new KnapsackConfiguration(3)));
        }

        ArrayList<Knapsack> filteredConfigurations = filter.filter(knapsacks);

        Assert.assertEquals("knapsacks count schould be 8", 8, filteredConfigurations.size());
        Assert.assertSame("filtered knapsack 0 should be generated knapsack 0", knapsacks.get(0), filteredConfigurations.get(0));
        Assert.assertSame("filtered knapsack 1 should be generated knapsack 1", knapsacks.get(1), filteredConfigurations.get(1));
        Assert.assertSame("filtered knapsack 2 should be generated knapsack 2", knapsacks.get(2), filteredConfigurations.get(2));
        Assert.assertSame("filtered knapsack 3 should be generated knapsack 3", knapsacks.get(3), filteredConfigurations.get(3));
        Assert.assertSame("filtered knapsack 4 should be generated knapsack 4", knapsacks.get(4), filteredConfigurations.get(4));
        Assert.assertSame("filtered knapsack 5 should be generated knapsack 5", knapsacks.get(5), filteredConfigurations.get(5));
        Assert.assertSame("filtered knapsack 6 should be generated knapsack 6", knapsacks.get(6), filteredConfigurations.get(6));
        Assert.assertSame("filtered knapsack 7 should be generated knapsack 7", knapsacks.get(7), filteredConfigurations.get(7));
    }

    @Test
    public void testTop25PercentFilter() {
        IKnapsacksFilter filter = new Top25PercentKnapsacksFilter();
        KnapsackService service = new KnapsackService();
        ArrayList<Knapsack> knapsacks = new ArrayList<>();

        // Create 8 Knapsacks for testing
        for(int i = 0; i < 8; i++) {
            knapsacks.add(service.create(new KnapsackConfiguration(3)));
        }

        ArrayList<Knapsack> filteredConfigurations = filter.filter(knapsacks);

        System.out.println(filteredConfigurations.get(0));

        Assert.assertEquals("knapsacks count schould be 2", 2, filteredConfigurations.size());
        Assert.assertSame("filtered knapsack 0 should be generated knapsack 0", knapsacks.get(0), filteredConfigurations.get(0));
        Assert.assertSame("filtered knapsack 0 should be generated knapsack 0", knapsacks.get(1), filteredConfigurations.get(1));
    }

    @Test
    public void testLast25PercentFilter() {
        IKnapsacksFilter filter = new Last25PercentKnapsacksFilter();
        KnapsackService service = new KnapsackService();
        ArrayList<Knapsack> knapsacks = new ArrayList<>();

        // Create 8 Knapsacks for testing
        for(int i = 0; i < 8; i++) {
            knapsacks.add(service.create(new KnapsackConfiguration(3)));
        }

        ArrayList<Knapsack> filteredConfigurations = filter.filter(knapsacks);

        System.out.println(filteredConfigurations.get(0));

        Assert.assertEquals("knapsacks count schould be 2", 2, filteredConfigurations.size());
        Assert.assertSame("filtered knapsack 0 should be generated knapsack 6", knapsacks.get(6), filteredConfigurations.get(0));
        Assert.assertSame("filtered knapsack 1 should be generated knapsack 7", knapsacks.get(7), filteredConfigurations.get(1));
    }

    @Test
    public void testMiddle50PercentFilter() {
        IKnapsacksFilter filter = new Middle50PercentKnapsacksFilter();
        KnapsackService service = new KnapsackService();
        ArrayList<Knapsack> knapsacks = new ArrayList<>();

        // Create 8 Knapsacks for testing
        for(int i = 0; i < 8; i++) {
            knapsacks.add(service.create(new KnapsackConfiguration(3)));
        }

        ArrayList<Knapsack> filteredConfigurations = filter.filter(knapsacks);

        System.out.println(filteredConfigurations.get(0));

        Assert.assertEquals("knapsacks count schould be 2", 4, filteredConfigurations.size());
        Assert.assertSame("filtered knapsack 2 should be generated knapsack 0", knapsacks.get(2), filteredConfigurations.get(0));
        Assert.assertSame("filtered knapsack 3 should be generated knapsack 1", knapsacks.get(3), filteredConfigurations.get(1));
        Assert.assertSame("filtered knapsack 4 should be generated knapsack 2", knapsacks.get(4), filteredConfigurations.get(2));
        Assert.assertSame("filtered knapsack 5 should be generated knapsack 3", knapsacks.get(5), filteredConfigurations.get(3));
    }
}
