package test.bruteforce;

import base.Knapsack;
import bruteforce.Bruteforce;
import bruteforce.IBruteforce;
import main.Application;
import main.Configuration;
import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class BruteforceTest {

    @Test
    public void testBruteForce() {
        /** Testumgenung aufgbauen */
        Application application = new Application();
        application.loadData();

        Bruteforce bruteforce = new Bruteforce();
        ArrayList<Knapsack> knapsacks = bruteforce.doBruteforce();

        knapsacks.forEach(knapsack -> System.out.println(knapsack.getTotalValue()));
    }
}
