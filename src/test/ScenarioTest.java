package test;

import crossover.KPointCrossover;
import crossover.OnePointCrossover;
import crossover.TwoPointCrossover;
import crossover.UniformCrossover;
import main.Application;
import main.Configuration;
import mutation.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import selection.RouletteWheelSelection;
import selection.TournamentSelection;

public class ScenarioTest {

    private static Application application;

    private void runApplication() {
        application.startupHSQLDB();
        application.execute("s01", "s02");
    }

    @BeforeClass
    public static void startup() {
        application = new Application();
        application.startupHSQLDB();
        application.loadData();
    }

    @AfterClass
    public static void shutdown() {
        application.shutdownHSQLDB();
    }


    @Test
    public void s01() {
        Configuration.instance.crossover = new OnePointCrossover();
        Configuration.instance.crossoverRatio = 0.8;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s02() {
        Configuration.instance.crossover = new OnePointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s03() {
        Configuration.instance.crossover = new OnePointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new DisplacementMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s04() {
        Configuration.instance.crossover = new OnePointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new InsertionMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s05() {
        Configuration.instance.crossover = new OnePointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new InversionMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s06() {
        Configuration.instance.crossover = new OnePointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ScrambleMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s07() {
        Configuration.instance.crossover = new TwoPointCrossover();
        Configuration.instance.crossoverRatio = 0.8;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s08() {
        Configuration.instance.crossover = new TwoPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s09() {
        Configuration.instance.crossover = new TwoPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new DisplacementMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s10() {
        Configuration.instance.crossover = new TwoPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new InsertionMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s11() {
        Configuration.instance.crossover = new TwoPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new InversionMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s12() {
        Configuration.instance.crossover = new TwoPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ScrambleMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s13() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.8;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s14() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s15() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.6;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }


    @Test
    public void s16() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

    @Test
    public void s17() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new DisplacementMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

    @Test
    public void s18() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new InsertionMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

    @Test
    public void s19() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new InversionMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

    @Test
    public void s20() {
        Configuration.instance.crossover = new KPointCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ScrambleMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

    @Test
    public void s21() {
        Configuration.instance.crossover = new UniformCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s22() {
        Configuration.instance.crossover = new UniformCrossover();
        Configuration.instance.crossoverRatio = 0.6;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new RouletteWheelSelection();
        runApplication();
    }

    @Test
    public void s23() {
        Configuration.instance.crossover = new UniformCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new ExchangeMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

    @Test
    public void s24() {
        Configuration.instance.crossover = new UniformCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new DisplacementMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

    @Test
    public void s25() {
        Configuration.instance.crossover = new UniformCrossover();
        Configuration.instance.crossoverRatio = 0.7;
        Configuration.instance.mutation = new InsertionMutation();
        Configuration.instance.mutationRatio = 0.0005;
        Configuration.instance.selection = new TournamentSelection();
        runApplication();
    }

}
