package bruteforce.validators;

import base.Knapsack;
import main.Configuration;

/**
 * Created by kevin on 17.02.2017.
 */
public class KnapsackValidator implements IValidator {

    /**
     * Checks whether the knapsack weight is smaller than the the max volume of a knapsack
     * @param knapsack
     * @return
     */
    public boolean isValid(Knapsack knapsack) {
        return knapsack.getTotalSize() <= Configuration.instance.maxVolumeKnapsack;
    }
}
