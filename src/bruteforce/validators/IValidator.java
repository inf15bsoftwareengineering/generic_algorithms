package bruteforce.validators;

import base.Knapsack;

/**
 * Created by kevin on 17.02.2017.
 */
public interface IValidator {
    boolean isValid(Knapsack knapsack);
}
