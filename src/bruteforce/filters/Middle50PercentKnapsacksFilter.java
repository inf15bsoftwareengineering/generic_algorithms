package bruteforce.filters;

import base.Knapsack;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class Middle50PercentKnapsacksFilter implements IKnapsacksFilter {
    @Override
    public ArrayList<Knapsack> filter(ArrayList<Knapsack> knapsacks) {
        ArrayList<Knapsack> filteredKnapsacks = new ArrayList<>();

        int startIndex = (int)(knapsacks.size() * 0.25);
        int endIndex = (int)(knapsacks.size() * 0.75);

        System.out.println("start " + startIndex + " end " + endIndex);

        for(int i = startIndex; i < endIndex; i++) {
            filteredKnapsacks.add(knapsacks.get(i));
        }

        return filteredKnapsacks;
    }
}
