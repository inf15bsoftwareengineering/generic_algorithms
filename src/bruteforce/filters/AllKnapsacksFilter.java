package bruteforce.filters;

import base.Knapsack;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class AllKnapsacksFilter implements IKnapsacksFilter {
    @Override
    public ArrayList<Knapsack> filter(ArrayList<Knapsack> knapsacks) {
        return knapsacks;
    }
}
