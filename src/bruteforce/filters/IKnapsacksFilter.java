package bruteforce.filters;

import base.Knapsack;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public interface IKnapsacksFilter {
    ArrayList<Knapsack> filter(ArrayList<Knapsack> knapsacks);
}
