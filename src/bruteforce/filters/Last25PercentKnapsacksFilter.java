package bruteforce.filters;

import base.Knapsack;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class Last25PercentKnapsacksFilter implements  IKnapsacksFilter {

    @Override
    public ArrayList<Knapsack> filter(ArrayList<Knapsack> knapsacks) {
        ArrayList<Knapsack> filteredKnapsacks = new ArrayList<>();

        int startIndex = (int)(knapsacks.size() * 0.75);

        for(int i = startIndex; i < knapsacks.size(); i++) {
            filteredKnapsacks.add(knapsacks.get(i));
        }

        return filteredKnapsacks;
    }
}
