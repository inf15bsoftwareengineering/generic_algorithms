package bruteforce.filters;

import base.Knapsack;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class Top25PercentKnapsacksFilter implements IKnapsacksFilter {

    @Override
    public ArrayList<Knapsack> filter(ArrayList<Knapsack> knapsacks) {
        ArrayList<Knapsack> filteredKnapsacks = new ArrayList<>();

        int endIndex = (int)(knapsacks.size() * 0.25);

        for(int i = 0; i < endIndex; i++) {
            filteredKnapsacks.add(knapsacks.get(i));
        }

        return filteredKnapsacks;
    }
}
