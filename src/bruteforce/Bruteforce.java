package bruteforce;

import base.Item;
import base.Knapsack;
import base.KnapsackConfiguration;
import bruteforce.generators.IConfigurationGenerator;
import bruteforce.generators.RandomConfigurationGenerator;
import bruteforce.generators.SequentialConfigurationGenerator;
import bruteforce.log.ILogger;
import bruteforce.log.Logger;
import bruteforce.services.KnapsackService;
import main.Application;
import main.Configuration;

import java.util.ArrayList;
import java.util.BitSet;

/**
 * Created by kevin on 16.02.2017.
 */
public class Bruteforce implements IBruteforce {

    private KnapsackService knapsackService = new KnapsackService();
    private ILogger logger = new Logger();

    public ArrayList<Knapsack> doBruteforce() {
        logger.logHeadline("Executing BruteforceTest");
        logger.logHeadline("Generating Configurations");

        ArrayList<KnapsackConfiguration> configurations = Configuration.instance.configurationGenerator.generate();
        ArrayList<Knapsack> knapsacks = knapsackService.create(configurations);

        return knapsacks;
    }

    public void printKnapsackValues(ArrayList<Knapsack> knapsacks) {
        System.out.println("=== Knapsacks values ===");

        for(Knapsack knapsack : knapsacks) {
            System.out.println("weight: " + knapsack.getTotalSize() + " value: " + knapsack.getTotalValue());
        }
    }
    public void printMostValuableKnapsack(ArrayList<Knapsack> knapsacks) {
        Knapsack mostValuable = knapsackService.filterMostValuable(knapsacks);

        System.out.println("=== Most Valuable Configuration ===");
        System.out.println(mostValuable.toString());
    }
}
