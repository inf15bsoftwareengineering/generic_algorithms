package bruteforce.log;

/**
 * Created by kevin on 17.02.2017.
 */
public class Logger implements ILogger {
    @Override
    public void log(String text) {
        System.out.println(text);
    }

    @Override
    public void logHeadline(String text) {
        System.out.println("=== " + text + " ===");
    }
}
