package bruteforce.log;

/**
 * Created by kevin on 17.02.2017.
 */
public interface ILogger {
    void log(String text);
    void logHeadline(String text);
}
