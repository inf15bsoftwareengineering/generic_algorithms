package bruteforce.generators;

import base.KnapsackConfiguration;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public interface IConfigurationGenerator {
    ArrayList<KnapsackConfiguration> generate();
}
