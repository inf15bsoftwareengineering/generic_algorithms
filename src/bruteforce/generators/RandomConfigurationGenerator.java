package bruteforce.generators;

import base.Knapsack;
import base.KnapsackConfiguration;
import bruteforce.services.KnapsackService;
import main.Configuration;
import random.MersenneTwisterFast;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class RandomConfigurationGenerator implements IConfigurationGenerator {
    private MersenneTwisterFast random = new MersenneTwisterFast();
    private KnapsackService knapsackService = new KnapsackService();

    public ArrayList<KnapsackConfiguration> generate() {
        ArrayList<KnapsackConfiguration> knapsackConfigurations = new ArrayList<KnapsackConfiguration>();

        // Generate n knapsacks
        for(int i = 0; i < Configuration.instance.bruteforceRandomConfigurationGeneratorIterations; i++) {
            KnapsackConfiguration configuration = new KnapsackConfiguration(Configuration.instance.populationSize);

            int failedIterations = 0;

            while(failedIterations < Configuration.instance.bruteforceRandomConfigurationGeneratorFailedIterations) {
                int bitIndex = random.nextInt(Configuration.instance.populationSize);

                // Randomly choose a bit
                configuration.set(bitIndex);

                /**
                 * Falls die neue Konfiguration das maximale Volumen überschreitet, setze das Bit zurück
                 * Dadurch überschreitet eine Konfiguration niemals das Maximalgewicht
                 */
                if(knapsackService.create(configuration).getTotalSize() > Configuration.instance.maxVolumeKnapsack) {
                    configuration.set(bitIndex, false);
                    failedIterations++;
                }
            }

            knapsackConfigurations.add(configuration);
        }
        return knapsackConfigurations;
    }
}
