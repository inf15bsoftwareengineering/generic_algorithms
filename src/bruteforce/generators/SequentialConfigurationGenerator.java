package bruteforce.generators;

import base.Knapsack;
import base.KnapsackConfiguration;
import bruteforce.services.KnapsackService;
import bruteforce.validators.IValidator;
import bruteforce.validators.KnapsackValidator;
import main.Configuration;

import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class SequentialConfigurationGenerator implements IConfigurationGenerator {

    private KnapsackService knapsackService = new KnapsackService();
    private IValidator knapsackValidator = new KnapsackValidator();

    public ArrayList<KnapsackConfiguration> generate() {
        ArrayList<KnapsackConfiguration> knapsackConfigurations = new ArrayList<KnapsackConfiguration>();

        // Create the first configuration
        KnapsackConfiguration knapsackConfiguration = new KnapsackConfiguration(Configuration.instance.populationSize);

        // Add the first configuration to the list
        knapsackConfigurations.add(knapsackConfiguration);

        // Add new configuration to the list by incrementing the bitset values of the latest configuration for each iteration
        for(long i = 0; i < Configuration.instance.sequentialConfigurationGeneratorIterations; i++) {

            // Create the next configuration
            knapsackConfiguration = (KnapsackConfiguration)knapsackConfiguration.clone();
            knapsackConfiguration.increment();

            // Add the new configuration to the list if its weight is smaller or equal to the MAX weight
            Knapsack knapsack = knapsackService.create(knapsackConfiguration);

            if(knapsackValidator.isValid(knapsack)) {
                knapsackConfigurations.add(knapsackConfiguration);
            }
        }

        return knapsackConfigurations;
    }
}
