package bruteforce.services;

import base.Item;
import base.KnapsackConfiguration;
import base.Knapsack;
import main.Application;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by kevin on 17.02.2017.
 */
public class KnapsackService {
    public Knapsack create(KnapsackConfiguration configuration) {
        Knapsack knapsack = new Knapsack();

        ArrayList<Item> items = (ArrayList)Application.getAllItemsFromCSV();

        // Check the configuration bits and select the corresponding item by the bit index
        for(int i = 0; i < configuration.bitsCount; i++) {
            if(configuration.get(i)) {
                knapsack.addItem(items.get(i));
            }
        }

        return knapsack;
    }
    public ArrayList<Knapsack> create(ArrayList<KnapsackConfiguration> configurations) {
        ArrayList<Knapsack> knapsacks = new ArrayList<Knapsack>();

        for(KnapsackConfiguration configuration : configurations) {
            knapsacks.add(create(configuration));
        }

        return knapsacks;
    }
    public Knapsack filterMostValuable(ArrayList<Knapsack> knapsacks) {
        Knapsack mostValuableKnapsack = null;

        for(Knapsack knapsack : knapsacks) {
            if(mostValuableKnapsack == null) {
                mostValuableKnapsack = knapsack;
            }
            else {
                if(knapsack.getTotalValue() > mostValuableKnapsack.getTotalValue()) {
                    mostValuableKnapsack = knapsack;
                }
            }
        }

        return mostValuableKnapsack;
    }
}
