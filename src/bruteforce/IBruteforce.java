package bruteforce;

import base.Knapsack;

import java.util.ArrayList;

/**
 * Created by kevin on 16.02.2017.
 */
public interface IBruteforce {
    ArrayList<Knapsack> doBruteforce();
}
