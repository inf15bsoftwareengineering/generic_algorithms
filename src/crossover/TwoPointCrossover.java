package crossover;

import base.Item;
import base.Knapsack;
import main.Configuration;

import java.util.ArrayList;
import java.util.List;

public class TwoPointCrossover implements ICrossover {
    public List<Knapsack> doCrossover(Knapsack parent1, Knapsack parent2) {
        List<Knapsack> returnValue = new ArrayList();

        if (Configuration.instance.crossoverRatio > Configuration.instance.randomNumberGenerator.nextDouble()) {

            int crossing_one = getRandom(parent1, parent2);
            int crossing_two = getRandom(parent1, parent2);
            while (crossing_one == crossing_two || crossing_one > crossing_two) {
                crossing_one = getRandom(parent1, parent2);
                crossing_two = getRandom(parent1, parent2);
            }

            Knapsack child1 = new Knapsack();
            Knapsack child2 = new Knapsack();

            List<Item> childOnePartTwo = parent1.getItems().subList(crossing_one, crossing_two);
            List<Item> childOnePartThree = parent1.getItems().subList(crossing_two, parent1.getItems().size());
            List<Item> childTwoPartTwo = parent2.getItems().subList(crossing_one, crossing_two);
            List<Item> childTwoPartThree = parent2.getItems().subList(crossing_two, parent2.getItems().size());

            for (int i = 0; i < crossing_one; i++) {
                child1.getItems().add(parent1.getItems().get(i));
                child2.getItems().add(parent2.getItems().get(i));
            }
            for (Item item : childTwoPartTwo) {
                if (!child1.getItems().contains(item)) {
                    child1.getItems().add(item);
                }
            }

            for (Item item : childOnePartThree) {
                if (!child1.getItems().contains(item)) {
                    child1.getItems().add(item);
                }
            }

            for (Item item : childOnePartTwo) {
                if (!child2.getItems().contains(item)) {
                    child2.getItems().add(item);
                }
            }

            for (Item item : childTwoPartThree) {
                if (!child2.getItems().contains(item)) {
                    child2.getItems().add(item);
                }
            }

            if (child1.getTotalSize() <= Configuration.instance.maxVolumeKnapsack) {
                returnValue.add(child1);
            }
            if (child2.getTotalSize() <= Configuration.instance.maxVolumeKnapsack) {
                returnValue.add(child2);
            }
        }
        return returnValue;
    }

    public String toString() {
        return getClass().getSimpleName();
    }

    private int getRandom(Knapsack parent1, Knapsack parent2) {
        return Configuration.instance.randomNumberGenerator.nextInt(Math.min(parent1.getItems().size(), parent2.getItems().size()));
    }
}