package crossover;

import base.Item;
import base.Knapsack;
import main.Configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OnePointCrossover implements ICrossover {
    public List<Knapsack> doCrossover(Knapsack parent1, Knapsack parent2) {
        // Returning an empty list if no child is created and a list with only the child if one is created
        List<Knapsack> returnValue = new ArrayList<>();
        if (Configuration.instance.crossoverRatio > Configuration.instance.randomNumberGenerator.nextDouble()) {
            int crossing = Configuration.instance.randomNumberGenerator.nextInt(Math.min(parent1.getItems().size(), parent2.getItems().size()));
            Knapsack child1 = new Knapsack();
            Knapsack child2 = new Knapsack();

            List<Item> parent1Part = parent1.getItems().subList(crossing, parent1.getItems().size());
            List<Item> parent2Part = parent2.getItems().subList(crossing, parent2.getItems().size());

            child1.getItems().addAll(parent1Part);
            child2.getItems().addAll(parent2Part);

            // Prevent duplicate items in the child knapsack
            for (Item item : parent2.getItems()) {
                if(parent2Part.contains(item)){
                    continue;
                }

                if (!child1.getItems().contains(item)) {
                    child1.getItems().add(item);
                }
            }


            for (Item item : parent1.getItems()) {
                if(parent1Part.contains(item)){
                    continue;
                }

                if (!child2.getItems().contains(item)) {
                    child2.getItems().add(item);
                }
            }

            if(child1.getTotalSize() <= Configuration.instance.maxVolumeKnapsack){
                returnValue.add(child1);
            }
            if(child2.getTotalSize() <= Configuration.instance.maxVolumeKnapsack){
                returnValue.add(child2);
            }
            return returnValue;
        } else {
            return returnValue;
        }
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}