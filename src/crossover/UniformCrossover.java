package crossover;

import base.Item;
import base.Knapsack;
import main.Configuration;

import java.util.ArrayList;
import java.util.List;

public class UniformCrossover implements ICrossover {
    public List<Knapsack> doCrossover(Knapsack parent1, Knapsack parent2) {

        List<Knapsack> returnValue = new ArrayList<>();
        if (Configuration.instance.crossoverRatio > Configuration.instance.randomNumberGenerator.nextDouble()) {
            Knapsack parent_one = new Knapsack();
            Knapsack parent_two = new Knapsack();
            Knapsack child1 = new Knapsack();
            Knapsack child2 = new Knapsack();

            parent_one.getItems().addAll(parent1.getItems());
            parent_two.getItems().addAll(parent2.getItems());

            // checking for Items that are in both Sacks
            for (Item item :
                    parent1.getItems()) {
                if (parent2.getItems().contains(item)) {
                    child1.getItems().add(item);
                    child2.getItems().add(item);
                    parent_one.getItems().remove(item);
                    parent_two.getItems().remove(item);
                }
            }

            while (!parent_one.getItems().isEmpty() && !parent_two.getItems().isEmpty()) {
                int randomNumber = Configuration.instance.randomNumberGenerator.nextInt(Math.min(parent_one.getItems().size(), parent_two.getItems().size()));
                if (child1.getItems().size() % 2 == 0) {
                    child1.getItems().add(parent_two.getItems().get(randomNumber));
                    child2.getItems().add(parent_one.getItems().get(randomNumber));
                } else {
                    child1.getItems().add(parent_one.getItems().get(randomNumber));
                    child2.getItems().add(parent_two.getItems().get(randomNumber));
                }
                parent_one.getItems().remove(randomNumber);
                parent_two.getItems().remove(randomNumber);
            }

            if (!parent_one.getItems().isEmpty() && parent_two.getItems().isEmpty()) {
                while (!parent_two.getItems().isEmpty()) {
                    int randomNumber = Configuration.instance.randomNumberGenerator.nextInt(parent_two.getItems().size());
                    if (parent_two.getItems().size() % 2 == 0) {
                        child1.getItems().add(parent_two.getItems().get(randomNumber));
                    } else {
                        child2.getItems().add(parent_two.getItems().get(randomNumber));
                    }
                    parent_two.getItems().remove(randomNumber);
                }
            }
            if (parent_one.getItems().isEmpty() && !parent_two.getItems().isEmpty()) {
                while (!parent_one.getItems().isEmpty()) {
                    int randomNumber = Configuration.instance.randomNumberGenerator.nextInt(parent_one.getItems().size());
                    if (parent_one.getItems().size() % 2 == 0) {
                        child2.getItems().add(parent_one.getItems().get(randomNumber));
                    } else {
                        child1.getItems().add(parent_one.getItems().get(randomNumber));
                    }
                    parent_one.getItems().remove(randomNumber);
                }
            }
            if (child1.getTotalSize() <= Configuration.instance.maxVolumeKnapsack) {
                returnValue.add(child1);
            }
            if (child2.getTotalSize() <= Configuration.instance.maxVolumeKnapsack) {
                returnValue.add(child2);
            }
        }
        return returnValue;
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}