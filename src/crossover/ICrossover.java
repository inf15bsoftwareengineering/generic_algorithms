package crossover;

import java.util.List;

import base.Knapsack;

public interface ICrossover {
    List<Knapsack> doCrossover(Knapsack parent1, Knapsack parent2);
}