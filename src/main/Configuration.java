package main;

import bruteforce.filters.AllKnapsacksFilter;
import bruteforce.filters.IKnapsacksFilter;
import bruteforce.generators.IConfigurationGenerator;
import bruteforce.generators.RandomConfigurationGenerator;
import bruteforce.generators.SequentialConfigurationGenerator;
import crossover.ICrossover;
import crossover.OnePointCrossover;
import mutation.IMutation;
import mutation.InsertionMutation;
import random.MersenneTwisterFast;
import selection.ISelection;
import selection.RouletteWheelSelection;

public enum Configuration {
    instance;
    public MersenneTwisterFast randomNumberGenerator = new MersenneTwisterFast(System.currentTimeMillis());

    public String fileSeparator = System.getProperty("file.separator");
    public String userDirectory = System.getProperty("user.dir");

    public String dataDirectory = userDirectory + fileSeparator + "data" + fileSeparator;
    public String dataFilePath = dataDirectory + "knapsack_instance.csv";

    public String configurationFilePath = userDirectory + fileSeparator +  "configuration/genetic_algorithm_knapsack.xml";

    public String databaseFile = dataDirectory + "datastore.db";

    public String scenarioId;
    // Default values which will get overwritten if you apply a new scenario
    public int populationSize = 150;
    public int maxVolumeKnapsack = 822;
    public int optimum = 1140;
    public double targetQuality = 0.95;

    public long maximumNumberOfEvaluations = 10000;
    public int maxUnchangedCycles = 1500;

    public int maxCrossoversKPoint = 5;

    public ICrossover crossover = new OnePointCrossover();
    public double crossoverRatio = 0.7;
    public IMutation mutation = new InsertionMutation();
    public double mutationRatio = 0.0005;
    public ISelection selection = new RouletteWheelSelection();

    public boolean buildStatstics = true;

    /**
     * This will make the algorithm run a lot longer because it will take longer to get to the point at which in every knapsack are the same items
     * but the results will be better, too
     */
    public boolean preventDuplicateChildInsertion = true;

    /***** Scenario options *****/

    /***** BruteforceTest options *****/
    // TODO test options should not be in the main configuration! Apply them only for the tests

    /**
     * Defines which generator will be used for the KnapsackConfigurations
     */
    public IConfigurationGenerator configurationGenerator = new RandomConfigurationGenerator();
    /**
     * Defines how many knapsacks configurations should be created when using the sequential configuration generator
     */
    public long sequentialConfigurationGeneratorIterations = 4000000L;
    /**
     * Defines how many knapsacks configurations should be created when using the random configuration generator
     */
    public long bruteforceRandomConfigurationGeneratorIterations = 5000;
    /**
     * Defines after how many failed iterations the random configuration generator stops to add an item to the current configuration
     */
    public long bruteforceRandomConfigurationGeneratorFailedIterations = 1000;
    /**
     * Defines which parts of the bruteforce-generated knapsacks should be selected for the evaluation
     */
    public IKnapsacksFilter bruteforceKnapsacksFilter = new AllKnapsacksFilter();


    @Override
    public String toString() {
        return "Configuration{" +
                "scenarioId='" + scenarioId + '\'' +
                ", crossover=" + crossover +
                ", mutation=" + mutation +
                ", selection=" + selection +
                '}';
    }
}
