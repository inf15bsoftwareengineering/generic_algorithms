package statistics;

import java.io.IOException;
import java.util.ArrayList;

public interface IStatistics {
    void writeCSVFile(ArrayList<Integer> data,String filename, String scenarioId, ArrayList<Integer> data2, String scenarioId2) throws IOException;
    void buildPlotRFile(String filename, String scenarioId) throws IOException;
    void buildMeasureRFile(String filename, String scenarioId, String scenarioId2) throws IOException;
    void buildBoxPlotRFile(String filename,String scenarioId, String scenarioId2) throws IOException;
    void buildTTestRFile(String workingDirectory,String filename01,String filename02) throws IOException;
    void buildStripChartRFile(String filename, String scenarioId, String scenarioId2) throws IOException;
    void buildAnalysisRFile(String filename, String scenarioId, String scenarioId2) throws IOException;
}