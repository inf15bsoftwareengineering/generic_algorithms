package statistics;

import data.HSQLDBManager;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class Statistics implements IStatistics {
    public static void main(String... args) {
        Statistics statistics = new Statistics();
        System.out.println("--- get Scenario");
        String scenarioId = HSQLDBManager.instance.getScenario().get(0);
        System.out.println("1. Szenario: "+ HSQLDBManager.instance.getScenario().get(0));
        System.out.println("--- get Scenario");
        String scenarioId2 = HSQLDBManager.instance.getScenario().get(1);
        System.out.println("2. Szenario: "+ HSQLDBManager.instance.getScenario().get(1));
        try {
            statistics.writeCSVFile(HSQLDBManager.instance.getData(scenarioId), "data.R", scenarioId, HSQLDBManager.instance.getData(scenarioId2), scenarioId2);
            statistics.buildPlotRFile("plot.R", scenarioId);
            statistics.buildMeasureRFile("measures.R", scenarioId, scenarioId2);
            statistics.buildTTestRFile("test.R", scenarioId, scenarioId2);
            statistics.buildBoxPlotRFile("box_plot.R", scenarioId, scenarioId2);
            statistics.buildStripChartRFile("stripchart.R", scenarioId, scenarioId2);
            statistics.buildAnalysisRFile("analysis.R", scenarioId, scenarioId2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeCSVFile(ArrayList<Integer> data,String filename, String scenarioId, ArrayList<Integer> data2, String scenarioId2) throws IOException {
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " <- c(");
        for (int i = 0; i < data.size(); i++) {
            bw.write(data.get(i) + ((i+1==data.size()) ? "" : ","));
        }
        bw.write(")\n\n");

        bw.write("data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " <- c(");
        for (int i = 0; i < data2.size(); i++) {
            bw.write(data2.get(i) + ((i+1==data2.size()) ? "" : ","));
        }
        bw.write(")\n");
        bw.close();
    }

    public void buildPlotRFile(String filename, String scenarioId) throws IOException {
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("cat(rep(\"\\n\",64))\n\n");
        bw.write("cat(getwd())\n");
        bw.write("pdf(\"plot.pdf\",height = 10, width = 10,paper = \"A4r\")\n");
        bw.write("iteration <- 1:length(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ")\n");
        bw.write("plot(iteration,data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",main=\"Generic Algorithm - TSP 280 [rws,pmx,0.7,hm,0.001]\",type=\"p\",cex = 0.1,xlab = \"iteration\",ylab = \"fitness\")\n");
        bw.write("dev.off()");
        bw.close();

    }

    public void buildMeasureRFile(String filename, String scenarioId, String scenarioId2) throws IOException {
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("cat(rep(\"\\n\",64))\n\n");
        bw.write("cat(\"*** median - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1)+ scenarioId2.charAt(2) + "***\\n\")\n");
        bw.write("cat(c(median(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + "),median(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ")))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** mean - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(c(round(mean(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + "),digits=2),round(mean(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + "),digits=2)))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** sd - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(c(round(sd(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + "),digits=2),round(sd(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + "),digits=2)))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** quantile (0.25) - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(c(round(quantile(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",prob=(0.25)),digits=2),round(quantile(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ",prob=(0.25)),digits=2)))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** quantile (0.5) - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(c(round(quantile(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",prob=(0.5)),digits=2),round(quantile(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ",prob=(0.5)),digits=2)))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** quantile (0.75) - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(c(round(quantile(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",prob=(0.75)),digits=2),round(quantile(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ",prob=(0.75)),digits=2)))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** range - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(c(max(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ") - min(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + "),max(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ") - min(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ")))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** interquartile range (0.75-0.25) - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(c(as.numeric(quantile(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",prob=(0.75))) - as.numeric(quantile(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",prob=(0.25))),as.numeric(quantile(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ",prob=(0.75))) - as.numeric(quantile(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ",prob=(0.25)))))\n");
        bw.write("cat(\"\\n\\n\")\n");
        bw.close();
    }

    public void buildBoxPlotRFile(String filename,String scenarioId, String scenarioId2) throws IOException {
        FileWriter fileWriter = new FileWriter(filename);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("cat(rep(\"\\n\",64))\n\n");
        bufferedWriter.write("cat(getwd())\n");
        bufferedWriter.write("pdf(\"boxplot.pdf\",height = 10,width = 10,paper = \"A4r\")\n");
        bufferedWriter.write("boxplot(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + ",main = \"Genetic Algorithm - TSP 280\",ylab = \"fitness\",names = c(\"data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " - rws,pmx,0.7,hm,0.0001\",\"data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " - rws,pmx,0.6,hm,0.0001\"),xlab =\"scenario(s)\")\n");
        bufferedWriter.write("dev.off()\n");
        bufferedWriter.close();
    }

    public void buildTTestRFile(String filename,String filename01,String filename02) throws IOException {
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("cat(rep(\"\\n\",64))\n\n");
        bw.write("cat(\"*** t-test - data" + filename01.charAt(1) + filename01.charAt(2) + " data" + filename02.charAt(1) + filename02.charAt(2) + " ***\\n\")\n");
        bw.write("print(t.test(data" + filename01.charAt(1) + filename01.charAt(2) + ",data" + filename02.charAt(1) + filename02.charAt(2) + "))\n");
        bw.write("cat(\"\\n\\n\")\n");
        bw.close();
    }

    public void buildStripChartRFile(String filename, String scenarioId, String scenarioId2) throws IOException {
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("cat(rep(\"\\n\",64))\n\n");
        bw.write("cat(getwd())\n");
        bw.write("pdf(\"stripchart.pdf\",height = 10,width = 10,paper = \"A4r\")\n");
        bw.write("stripchart(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + ",method = \"stack\",main = \"Genetic Algorithm - TSP 280\",xlab = \"fitness\",ylab = \"occurrences\")\n");
        bw.write("dev.off()\n");
        bw.close();
    }

    public void buildAnalysisRFile(String filename, String scenarioId, String scenarioId2) throws IOException {
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("cat(rep(\"\\n\",64))\n\n");
        bw.write("cat(\"*** most frequent fitness values - data" + scenarioId.charAt(1) + scenarioId.charAt(2) + " ***\\n\")\n");
        bw.write("cat(tail(names(sort(table(data" + scenarioId.charAt(1) + scenarioId.charAt(2) + "))),3))\n");
        bw.write("cat(\"\\n\\n\")\n\n");
        bw.write("cat(\"*** most frequent fitness values - data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + " ***\\n\")\n");
        bw.write("cat(tail(names(sort(table(data" + scenarioId2.charAt(1) + scenarioId2.charAt(2) + "))),3))\n");
        bw.write("cat(\"\\n\\n\")\n");
        bw.close();
    }
}