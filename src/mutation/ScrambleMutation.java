package mutation;

import base.Item;
import base.Knapsack;
import random.MersenneTwisterFast;

import java.util.ArrayList;
import java.util.List;

public class ScrambleMutation implements IMutation {
    private MersenneTwisterFast random = new MersenneTwisterFast();

    public Knapsack doMutation(Knapsack knapsack) {
        List<Item> list = knapsack.getItems();
        int size = list.size();

        //create two random numbers
        int indexOne = random.nextInt(size);
        int indexTwo = random.nextInt(size);
        while(indexTwo == indexOne) {
            indexTwo = random.nextInt(size);
        }
        if(indexTwo < indexOne) {
            int change = indexOne;
            indexOne = indexTwo;
            indexTwo = change;
        }
        //create new Knapsack for mutation
        Knapsack knNew = new Knapsack();
        for(int i=0; i<indexOne; i++) {
            knNew.addItem(list.get(i));
        }

        //items scrambled into random order
        ArrayList<Item> roulette = new ArrayList<>();
        for(int i=indexOne; i<=indexTwo; i++) {
            roulette.add(list.get(i));
        }
        for(int i=roulette.size(); i>0; i--) {
            double rand = random.nextDouble(true, true);
            int win = (int) (rand * i);
            knNew.addItem(roulette.get(win));
            roulette.remove(win);
        }

        for(int i=indexTwo+1; i<size; i++) {
            knNew.addItem(list.get(i));
        }

        return knNew;
    }


    public String toString() {
        return getClass().getSimpleName();
    }
}