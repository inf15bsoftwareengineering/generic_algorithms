package mutation;

import base.Item;
import base.Knapsack;
import main.Configuration;
import random.MersenneTwisterFast;

import java.util.List;

public class ExchangeMutation implements IMutation {
    private MersenneTwisterFast random = Configuration.instance.randomNumberGenerator;

    public Knapsack doMutation(Knapsack knapsack) {
        // Deep copy
        Knapsack copy = knapsack.clone();
        List<Item> list = copy.getItems();
        int size = list.size();

        // Chose two different randoms
        int indexOne = random.nextInt(size);
        int indexTwo = random.nextInt(size);
        while (indexTwo == indexOne) {
            indexTwo = random.nextInt(size);
        }

        // Swap items
        Item cachedItem = copy.getItems().get(indexOne);
        copy.getItems().set(indexOne, copy.getItems().get(indexTwo));
        copy.getItems().set(indexTwo, cachedItem);
        return copy;
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}