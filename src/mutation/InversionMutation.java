package mutation;

import base.Item;
import base.Knapsack;
import random.MersenneTwisterFast;

import java.util.ArrayList;
import java.util.List;

public class InversionMutation implements IMutation {
    private MersenneTwisterFast random = new MersenneTwisterFast();

    public Knapsack doMutation(Knapsack knapsack) {
        List<Item> list = knapsack.getItems();
        int size = list.size();
        
        //select two random numbers
        int indexOne = random.nextInt(size);
        int indexTwo = random.nextInt(size);
        while(indexTwo >= indexOne-1 && indexTwo < indexOne+1) {
            indexTwo = random.nextInt(size);
        }
        if(indexTwo < indexOne) {
            int i = indexOne;
            indexOne = indexTwo;
            indexTwo = i;
        }
        //invert order of items in selected scope
        Knapsack knNew = new Knapsack();
        for(int i=0; i<size; i++) {
            if(i>=indexOne && i<indexTwo) {
                for(int j=indexTwo; j>=indexOne; j--) {
                    knNew.addItem(list.get(j));
                }
                i = indexTwo;
            } else {
                knNew.addItem(list.get(i));
            }
        }
        return knNew;
    }
    public String toString() {
        return getClass().getSimpleName();
    }
}