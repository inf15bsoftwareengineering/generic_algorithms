package mutation;

import base.Item;
import base.Knapsack;
import random.MersenneTwisterFast;

import java.util.ArrayList;
import java.util.List;

public class DisplacementMutation implements IMutation {
    private MersenneTwisterFast random = new MersenneTwisterFast();

    public Knapsack doMutation(Knapsack knapsack) {
        List<Item> list = knapsack.getItems();
        int size = list.size();

        int indexOne = random.nextInt(size);
        int indexTwo = random.nextInt(size);
        while(indexTwo >= indexOne-1 && indexTwo < indexOne+1) {
            indexTwo = random.nextInt(size);
        }
        if(indexTwo < indexOne) {
            int i = indexOne;
            indexOne = indexTwo;
            indexTwo = i;
        }

        int shiftIndex = random.nextInt(size-(indexTwo-indexOne));
        while(shiftIndex==indexOne) {
            shiftIndex = random.nextInt(size-(indexTwo-indexOne));
        }

        Knapsack knNew = new Knapsack();
        if(indexOne<shiftIndex) {
            //load Knapsack with items until indexOne - 1
            for (int i = 0; i < indexOne; i++) {
                knNew.addItem(list.get(i));
            }
            //load Knapsaack with items behind indexOne
            for (int i = indexOne+1; i < shiftIndex+1; i++) {
                knNew.addItem(list.get(i + indexTwo - indexOne));
            }
            int count = 0;
            //shift Knapsack
            for (int i = shiftIndex; i < shiftIndex+1 + indexTwo - indexOne; i++) {
                knNew.addItem(list.get(indexOne + count));
                count++;
            }
            //load Knapsack with remaining items
            for (int i = shiftIndex+1 + indexTwo - indexOne; i < list.size(); i++) {
                knNew.addItem(list.get(i));
            }
        } else {
            for(int i=0; i<shiftIndex; i++) {
                knNew.addItem(list.get(i));
            }
            for(int i=indexOne; i<indexTwo+1; i++) {
                knNew.addItem(list.get(i));
            }
            for(int i=shiftIndex; i<indexOne; i++) {
                knNew.addItem(list.get(i));
            }
            for(int i=indexTwo+1; i<list.size(); i++) {
                knNew.addItem((list.get(i)));
            }
        }
        return knNew;
    }

    public Knapsack doMutationTest(Knapsack knapsack, int indexOne, int indexTwo, int shiftIndex) {
        List<Item> list = knapsack.getItems();

        Knapsack knNew = new Knapsack();
        if(indexOne<shiftIndex) {
            //Anfang einfuegen
            for (int i = 0; i < indexOne; i++) {
                knNew.addItem(list.get(i));
                System.out.println(list.get(i).getName());
            }
            //Intervall mit hinteren Werten auffuellen
            for (int i = indexOne+1; i < shiftIndex+1; i++) {
                knNew.addItem(list.get(i + indexTwo - indexOne));
                System.out.println(list.get(i + indexTwo - indexOne).getName());
            }
            int count = 0;
            //Intervall verschieben
            for (int i = shiftIndex; i < shiftIndex+1 + indexTwo - indexOne; i++) {
                knNew.addItem(list.get(indexOne + count));
                System.out.println(list.get(indexOne + count).getName());
                count++;
            }
            //Rest auffuellen
            for (int i = shiftIndex+1 + indexTwo - indexOne; i < list.size(); i++) {
                knNew.addItem(list.get(i));
                System.out.println(list.get(i).getName());
            }
        } else {
            for(int i=0; i<shiftIndex; i++) {
                knNew.addItem(list.get(i));
                System.out.println(list.get(i).getName());
            }
            for(int i=indexOne; i<indexTwo+1; i++) {
                knNew.addItem(list.get(i));
                System.out.println(list.get(i).getName());
            }
            for(int i=shiftIndex; i<indexOne; i++) {
                knNew.addItem(list.get(i));
                System.out.println(list.get(i).getName());
            }
            for(int i=indexTwo+1; i<list.size(); i++) {
                knNew.addItem((list.get(i)));
                System.out.println(list.get(i).getName());
            }
        }
        return knNew;
    }


    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}