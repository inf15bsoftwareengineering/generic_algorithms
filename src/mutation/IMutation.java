package mutation;

import base.Knapsack;

public interface IMutation {
    Knapsack doMutation(Knapsack knapsack);
}