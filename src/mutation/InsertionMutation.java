package mutation;

import base.Item;
import base.Knapsack;
import random.MersenneTwisterFast;

import java.util.ArrayList;
import java.util.List;

public class InsertionMutation implements IMutation {
    private MersenneTwisterFast random = new MersenneTwisterFast();

    public Knapsack doMutation(Knapsack knapsack) {
        List<Item> list = knapsack.getItems();
        int size = list.size();
        int indexOne = random.nextInt(size);
        int indexTwo = random.nextInt(size);
        while(indexTwo == indexOne) {
            indexTwo = random.nextInt(size);
        }

        //change items and write them into Knapsack
        Knapsack knNew = new Knapsack();
        if(indexOne<indexTwo) {
            for(int i=0; i<indexOne; i++) {
                knNew.addItem(list.get(i));
            }
            for(int i=indexOne+1; i<indexTwo+1; i++) {
                knNew.addItem(list.get(i));
            }
            knNew.addItem(list.get(indexOne));
            for(int i=indexTwo+1; i<size; i++) {
                knNew.addItem(list.get(i));
            }
        } else {
            for(int i=0; i<indexTwo; i++) {
                knNew.addItem(list.get(i));
            }
            knNew.addItem(list.get(indexOne));
            for(int i=indexTwo; i<indexOne; i++) {
                knNew.addItem(list.get(i));
            }
            for(int i=indexOne+1; i<size; i++) {
                knNew.addItem(list.get(i));
            }
        }

        return knNew;
    }
    public String toString() {
        return getClass().getSimpleName();
    }
}