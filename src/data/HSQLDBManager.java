package data;

import java.sql.*;
import java.util.ArrayList;

import main.Configuration;

public enum HSQLDBManager {
    instance;

    private Connection connection;
    private String driverName = "jdbc:hsqldb:";
    private String username = "SA";
    private String password = "";

    public void startup() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            String databaseURL = driverName + Configuration.instance.databaseFile;
            connection = DriverManager.getConnection(databaseURL,username,password);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void init() {
        dropTable();
        createTable();
    }

    public synchronized void update(String sqlStatement) {
        try {
            Statement statement = connection.createStatement();
            int result = statement.executeUpdate(sqlStatement);
            if (result == -1)
                System.out.println("error executing " + sqlStatement);
            statement.close();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }

    public void dropTable() {
        System.out.println("--- dropTable");

        StringBuilder sqlStringBuilder = new StringBuilder();
        sqlStringBuilder.append("DROP TABLE data");
        System.out.println("sqlStringBuilder : " + sqlStringBuilder.toString());

        update(sqlStringBuilder.toString());
    }

    /**
     * Gibt Alle verschiedenen Szenarien aus der Datenbank zurück
     * @return
     */
    public ArrayList<String> getScenario () {
        ArrayList<String> sid = new ArrayList<String>();

        try {
            Class.forName("org.hsqldb.jdbcDriver");
            connection = DriverManager.getConnection(driverName + Configuration.instance.databaseFile, username, password);
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(
                    "SELECT DISTINCT scenarioId FROM data");

            while (result.next()) {
                String id = result.getString("scenarioId");
                sid.add(id);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sid;
    }


        public void createTable() {
        StringBuilder sqlStringBuilder = new StringBuilder();
        sqlStringBuilder.append("CREATE TABLE data ").append(" ( ");
        sqlStringBuilder.append("ID BIGINT NOT NULL").append(",");
        sqlStringBuilder.append("scenarioId VARCHAR(3) NOT NULL").append(",");
        sqlStringBuilder.append("fitnessValue INTEGER NOT NULL").append(",");
        sqlStringBuilder.append("PRIMARY KEY (ID)");
        sqlStringBuilder.append(" )");
        update(sqlStringBuilder.toString());
    }

    public String buildSQLStatement(long ID,String scenarioId,int fitnessValue) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO data (ID,scenarioId,fitnessValue) VALUES (");
        stringBuilder.append(ID).append(",'");
        stringBuilder.append(scenarioId).append("',");
        stringBuilder.append("").append(fitnessValue).append("");
        stringBuilder.append(")");
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    public void insert(long ID,String scenarioId,int fitnessValue) {
        update(buildSQLStatement(ID,scenarioId,fitnessValue));
    }

    /**
     * Gibt Alle Szenarien und ihre korrespondierenden FitnessValues zurück
     * @return
     */
    public ArrayList<Integer> getData(String scenarioId) {
        ArrayList<Integer> data = new ArrayList<Integer>();

        try {
            Class.forName("org.hsqldb.jdbcDriver");
            connection = DriverManager.getConnection(driverName + Configuration.instance.databaseFile,username,password);
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(
                    "SELECT fitnessValue FROM data WHERE scenarioId = '" + scenarioId + "'");

            while(result.next()){

                int fitnessValue = result.getInt("fitnessValue");
                System.out.println(result);
                data.add(fitnessValue);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        return data;
    }


    public void shutdown() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("SHUTDOWN");
            connection.close();
        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
    }


}