package base;

import main.Configuration;

import java.util.*;
import java.util.stream.Collectors;

public class Population {

    private List<Knapsack> selection = new ArrayList<>();
    private long populationFitness = 0;

    /**
     * This a store of all item maps for every knapsack to prevent the creation of the list on each evolution step
     */
    private static Map<Knapsack, List<String>> itemMap = new HashMap<>();

    public float getPopulationFitness() {
        return populationFitness;
    }

    public Population evolve() {
        List<Knapsack> currentSelected = Configuration.instance.selection.doSelection(selection);
        List<Knapsack> children = doAllCrossovers(currentSelected);

        for (int i = 0; i < children.size(); i++) {
            if (Configuration.instance.randomNumberGenerator.nextDouble() < Configuration.instance.mutationRatio) {
                Knapsack knapsack = Configuration.instance.mutation.doMutation(children.get(i));
                children.set(i, knapsack);
            }
        }

        // Get population to initial size
        Population nextGeneration = new Population();
        nextGeneration.getSelection().addAll(this.getSelection());

        // This block will prevent to use childs which have the same items like any other Knapsack in the selection
        if (Configuration.instance.preventDuplicateChildInsertion) {
            for (Knapsack child : children) {
                List<String> childItems = child.getItems().stream().distinct().map(Item::getName).collect(Collectors.toList());
                childItems.sort(Comparator.comparing(Integer::valueOf));
                boolean available = false;
                for (Knapsack selected : getSelection()) {
                    List<String> selectedItems = itemMap.get(selected);
                    if (selectedItems == null) {
                        selectedItems = selected.getItems().stream().distinct().map(Item::getName).collect(Collectors.toList());
                        selectedItems.sort(Comparator.comparing(Integer::valueOf));
                        itemMap.put(selected, selectedItems);
                    }

                    if (childItems.equals(selectedItems)) {
                        available = true;
                        break;
                    }
                }
                if (!available) {
                    nextGeneration.getSelection().add(child);
                }
            }
        } else {
            nextGeneration.getSelection().addAll(children);
        }


        Collections.sort(nextGeneration.getSelection());
        if (nextGeneration.getSelection().size() > Configuration.instance.populationSize) {
            for (int i = Configuration.instance.populationSize; i < nextGeneration.getSelection().size(); i++) {
                itemMap.remove(nextGeneration.getSelection().get(i));
            }
            nextGeneration.setSelection(nextGeneration.getSelection().subList(0, Configuration.instance.populationSize));
        }

        nextGeneration.recalculate();
        return nextGeneration;
    }

    private List<Knapsack> doAllCrossovers(List<Knapsack> parents) {
        List<Knapsack> children = new ArrayList<>();


        // Build pairs as long as possible
        while (parents.size() / 2 > 0) {
            // somehow there were many null elements in the list
            parents.removeAll(Collections.singleton(null));
            if (parents.size() < 2) {
                break;
            }

            int randomNumber = Configuration.instance.randomNumberGenerator.nextInt(parents.size());
            Knapsack parent1 = parents.get(randomNumber);
            parents.remove(parent1);

            randomNumber = Configuration.instance.randomNumberGenerator.nextInt(parents.size());
            Knapsack parent2 = parents.get(randomNumber);
            parents.remove(parent2);

            children.addAll(Configuration.instance.crossover.doCrossover(parent1, parent2));
        }
        return children;
    }

    public List<Knapsack> getSelection() {
        return selection;
    }

    public void setSelection(List<Knapsack> selection) {
        this.selection = selection;
    }

    private void calculatePopulationFitness() {
        for (Knapsack k : selection) {
            populationFitness += k.getTotalValue();
        }
    }

    private void calculateSelectionProbabilities() {
        for (Knapsack k : selection) {
            k.selectionProbability(this.populationFitness);
        }
    }

    public Knapsack getFittest() {
        Knapsack returnValue = null;
        for (Knapsack sack : selection) {
            if (returnValue == null) {
                returnValue = sack;
            } else {
                if (returnValue.getTotalValue() < sack.getTotalValue()) {
                    returnValue = sack;
                }
            }

        }
        return returnValue;
    }

    public void recalculate() {
        calculatePopulationFitness();
        calculateSelectionProbabilities();
    }
}
