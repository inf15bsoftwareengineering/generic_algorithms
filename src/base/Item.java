package base;

public class Item implements Comparable<Item>, Cloneable{
    private String name;
    private int size, value;

    public Item(String name, int size, int value) {
        this.name = name;
        this.size = size;
        this.value = value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return name.equals(item.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public String getName() {
        return name;
    }

    public Item clone(){
        return new Item(this.name, this.size, this.value);
    }

    public int getSize() {
        return size;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int compareTo(Item o) {
        try {
            return Integer.valueOf(this.getName()).compareTo(Integer.valueOf(o.getName()));
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}