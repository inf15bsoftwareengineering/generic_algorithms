package base;

import main.Application;
import main.Configuration;

import java.util.ArrayList;
import java.util.List;

public class Knapsack implements Comparable<Knapsack> {
    private float selectionProbability;
    private List<Item> items = new ArrayList<>();

    public Knapsack() {
    }

    public Knapsack(Knapsack knapsack) {
        for (Item item : knapsack.getItems())
            items.add(item);
    }

    public Knapsack clone() {
        Knapsack sack = new Knapsack();
        for (Item item : items) {
            sack.addItem(item.clone());
        }
        return sack;
    }

    public int getTotalValue() {
        int total = 0;

        for (Item item : items) total += item.getValue();
        return total;
    }

    public int getTotalSize() {
        int total = 0;

        for (Item item : items) total += item.getSize();
        return total;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Item item : items)
            stringBuilder.append(item.getName() + " ");
        stringBuilder.append(" - ").append("total value: ").append(getTotalValue()).append(" total weight: ").append(getTotalSize());

        return stringBuilder.toString();
    }

    public void selectionProbability(long populationFitness) {
        selectionProbability = (float) (this.getTotalValue()) / (float) (populationFitness);
    }

    public static Knapsack generateRandom() {
        Knapsack randomKnapsack = new Knapsack();
        List<Item> itemList = new ArrayList<>(Application.getAllItemsFromCSV());

        while (randomKnapsack.getTotalSize() < Configuration.instance.maxVolumeKnapsack) {
            int random = Configuration.instance.randomNumberGenerator.nextInt(itemList.size());
            Item randomItem = itemList.get(random);
            if (randomItem.getSize() + randomKnapsack.getTotalSize() <= Configuration.instance.maxVolumeKnapsack) {
                randomKnapsack.addItem(randomItem);
                itemList.remove(random);
            } else return randomKnapsack;
        }

        return randomKnapsack;
    }

    @Override
    public int compareTo(Knapsack o) {
        if (this.getTotalValue() > o.getTotalValue()) {
            return -1;
        } else if (this.getTotalValue() < o.getTotalValue()) {
            return 1;
        }
        return 0;
    }

    public List<Item> getItems() {
        return items;
    }

    public float getSelectionProbability() {
        return selectionProbability;
    }
}