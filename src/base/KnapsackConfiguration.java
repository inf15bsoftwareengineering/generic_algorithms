package base;

import java.util.BitSet;

/**
 * Created by kevin on 17.02.2017.
 */
public class KnapsackConfiguration extends BitSet {

    public int bitsCount;

    public KnapsackConfiguration(int nbits) {
        super(nbits);
        bitsCount = nbits;
    }

    public void increment() {
        for (int i = 0; i < this.size(); i++) {
            if (!this.get(i)) {
                this.set(i);
                this.set(0, i, false);

                break;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < bitsCount; i++) {
            if (this.get(i)) {
                builder.append("1");
            } else {
                builder.append("0");
            }
        }

        return builder.toString();
    }
}