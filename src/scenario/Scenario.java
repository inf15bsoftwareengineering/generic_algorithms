package scenario;

import crossover.ICrossover;
import main.Configuration;
import mutation.IMutation;
import scenario.adapter.CrossoverXmlAdapter;
import scenario.adapter.MutationXmlAdapter;
import scenario.adapter.SelectionXmlAdapter;
import selection.ISelection;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "scenario")
@XmlAccessorType(XmlAccessType.FIELD)
public class Scenario {

    @XmlAttribute
    private String id;

    @XmlJavaTypeAdapter(value = CrossoverXmlAdapter.class, type = ICrossover.class)
    private ICrossover crossover;

    @XmlElement
    private double crossoverRatio;

    @XmlJavaTypeAdapter(value = MutationXmlAdapter.class, type = IMutation.class)
    private IMutation mutation;

    @XmlElement
    private double mutationRatio;

    @XmlJavaTypeAdapter(value = SelectionXmlAdapter.class, type = ISelection.class)
    private ISelection selection;

    @XmlElement
    private boolean buildStatistics;

    @XmlElement
    private String evaluation;

    @XmlElement
    private boolean isEvaluated;

    @XmlElement
    private long maximumNumberOfEvaluations;


    public void applyConfiguration(){
        Configuration.instance.scenarioId = id;
        Configuration.instance.crossover = crossover;
        Configuration.instance.crossoverRatio = crossoverRatio;
        Configuration.instance.mutation = mutation;
        Configuration.instance.mutationRatio = mutationRatio;
        Configuration.instance.selection = selection;
        Configuration.instance.buildStatstics = buildStatistics;
        Configuration.instance.maximumNumberOfEvaluations = maximumNumberOfEvaluations;

        // TODO what is isevaluated and evaluation?????
    }

    @Override
    public String toString() {
        return "\nScenario{" +
                "id='" + id + '\'' +
                ", crossover=" + crossover +
                ", crossoverRatio=" + crossoverRatio +
                ", mutation=" + mutation +
                ", mutationRatio=" + mutationRatio +
                ", selection=" + selection +
                ", buildStatistics=" + buildStatistics +
                ", evaluation='" + evaluation + '\'' +
                ", maximumNumberOfEvaluations=" + maximumNumberOfEvaluations +
                '}';
    }

    public boolean isEvaluated() {
        return isEvaluated;
    }

    public void setEvaluated(boolean evaluated) {
        isEvaluated = evaluated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ICrossover getCrossover() {
        return crossover;
    }

    public void setCrossover(ICrossover crossover) {
        this.crossover = crossover;
    }

    public double getCrossoverRatio() {
        return crossoverRatio;
    }

    public void setCrossoverRatio(double crossoverRatio) {
        this.crossoverRatio = crossoverRatio;
    }

    public IMutation getMutation() {
        return mutation;
    }

    public void setMutation(IMutation mutation) {
        this.mutation = mutation;
    }

    public double getMutationRatio() {
        return mutationRatio;
    }

    public void setMutationRatio(double mutationRatio) {
        this.mutationRatio = mutationRatio;
    }

    public ISelection getSelection() {
        return selection;
    }

    public void setSelection(ISelection selection) {
        this.selection = selection;
    }

    public boolean isBuildStatistics() {
        return buildStatistics;
    }

    public void setBuildStatistics(boolean buildStatistics) {
        this.buildStatistics = buildStatistics;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public long getMaximumNumberOfEvaluations() {
        return maximumNumberOfEvaluations;
    }

    public void setMaximumNumberOfEvaluations(long maximumNumberOfEvaluations) {
        this.maximumNumberOfEvaluations = maximumNumberOfEvaluations;
    }
}
