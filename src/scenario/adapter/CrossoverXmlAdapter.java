package scenario.adapter;


import crossover.*;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CrossoverXmlAdapter extends XmlAdapter<String, ICrossover> {

    @Override
    public ICrossover unmarshal(String v) throws Exception {
        switch (v) {
            case "OnePoint":
                return new OnePointCrossover();
            case "TwoPoint":
                return new TwoPointCrossover();
            case "K-Point":
                return new KPointCrossover();
            case "Uniform":
                return new UniformCrossover();
        }
        return null;
    }

    @Override
    public String marshal(ICrossover v) throws Exception {
        if (v instanceof OnePointCrossover) {
            return "OnePoint";
        } else if (v instanceof TwoPointCrossover) {
            return "TwoPoint";
        } else if (v instanceof KPointCrossover) {
            return "K-Point";
        } else if (v instanceof UniformCrossover) {
            return "Uniform";
        } else {
            return "";
        }
    }
}
