package scenario.adapter;


import mutation.*;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MutationXmlAdapter extends XmlAdapter<String, IMutation> {

    @Override
    public IMutation unmarshal(String v) throws Exception {
        switch (v) {
            case "Exchange":
                return new ExchangeMutation();
            case "Displacement":
                return new DisplacementMutation();
            case "Insertion":
                return new InsertionMutation();
            case "Inversion":
                return new InversionMutation();
            case "Scramble":
                return new ScrambleMutation();
        }
        return null;
    }

    @Override
    public String marshal(IMutation v) throws Exception {
        if (v instanceof ExchangeMutation) {
            return "Exchange";
        } else if (v instanceof DisplacementMutation) {
            return "Displacement";
        } else if (v instanceof InsertionMutation) {
            return "Insertion";
        } else if (v instanceof InversionMutation) {
            return "Inversion";
        } else if (v instanceof ScrambleMutation) {
            return "Scramble";
        } else {
            return "";
        }
    }
}
