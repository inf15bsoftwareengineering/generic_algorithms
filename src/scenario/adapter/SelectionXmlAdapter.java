package scenario.adapter;


import mutation.*;
import selection.ISelection;
import selection.RouletteWheelSelection;
import selection.TournamentSelection;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class SelectionXmlAdapter extends XmlAdapter<String, ISelection> {

    @Override
    public ISelection unmarshal(String v) throws Exception {
        switch (v) {
            case "RouletteWheel":
                return new RouletteWheelSelection();
            case "Tournament":
                return new TournamentSelection();
        }
        return null;
    }

    @Override
    public String marshal(ISelection v) throws Exception {
        if (v instanceof RouletteWheelSelection) {
            return "RouletteWheel";
        } else if (v instanceof TournamentSelection) {
            return "Tournament";
        } else {
            return "";
        }
    }
}
